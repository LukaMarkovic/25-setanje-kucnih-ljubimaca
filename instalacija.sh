#!/bin/bash

echo Installation started!
cd client
npm install
cd ..

cd server
npm install express
npm install mongoose

cd database
mongorestore
