const express = require('express');

const router = express.Router();

const controller = require('../../controllers/walkersController');

router.get('/', controller.getWalkers);
router.get('/:id', controller.getWalkerById);

router.post('/', controller.createWalker);
router.post('/:id', controller.updateWalkerById);
router.delete('/:id', controller.deleteWalkerById);

router.use((req, res, next) => {
    const unknownMethod = req.method;
    const unknownPath = req.path;
    res.status(500).json({'message': 'Nepoznat zahtev ${unknownMethod} ka ${unknownPath}!'});
});


module.exports = router