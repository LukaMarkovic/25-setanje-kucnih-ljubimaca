const express = require('express');

const router = express.Router();

const controller = require('../../controllers/adminsController');

router.get('/', controller.getAdmins);
router.get('/:id', controller.getAdminById);
router.post('/', controller.findAdminByEmail);

router.use((req, res, next) => {
    const unknownMethod = req.method;
    const unknownPath = req.path;
    res.status(500).json({'message': 'Nepoznat zahtev ${unknownMethod} ka ${unknownPath}!'});
});


module.exports = router