const http = require('http');
const app = require('./api');

const port = 3000;

const server = http.createServer(app);

server.listen(port, () => {
    console.log('Apikaija je aktivna');
});

