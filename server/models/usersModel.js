const mongoose = require('mongoose');

const petSchema = new mongoose.Schema({ 
    _id: mongoose.Schema.Types.ObjectId,
    name: String,
    breed: String,
    bday: Number
 });

const usersSchema = new mongoose.Schema({
    _id: mongoose.Schema.Types.ObjectId,
    name: {
        type: String,
        required: true  
    },
    surname: {
        type: String,
        required: true  
    },
    password: {
        type: String,
        required: true  
    },
    address: {
        type: String,
        required: true
    },
    email: {
        type: String,
        required: true  
    },
    pets: [petSchema]
});

const usersModel = mongoose.model('users', usersSchema);

module.exports = usersModel;