const mongoose = require('mongoose');

const adminsSchema = new mongoose.Schema({
    _id: mongoose.Schema.Types.ObjectId,
    name: {
        type: String,
        required: true  
    },
    surname: {
        type: String,
        required: true  
    },
    password: {
        type: String,
        required: true  
    },
    email: {
        type: String,
        required: true  
    }
});

const adminsModel = mongoose.model('admins', adminsSchema);

module.exports = adminsModel;