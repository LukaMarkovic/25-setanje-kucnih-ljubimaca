const mongoose = require('mongoose');

const requestsSchema = new mongoose.Schema({ 
    _id: mongoose.Schema.Types.ObjectId,
    timeDate: String,
    ownerId: mongoose.Schema.Types.ObjectId,
    walkerId: mongoose.Schema.Types.ObjectId,
    petId: mongoose.Schema.Types.ObjectId
 });



const requestsModel = mongoose.model('requests', requestsSchema);

module.exports = requestsModel;