const express = require('express');
const { urlencoded, json} = require('body-parser');
const mongoose = require('mongoose');



const app = express();

//povezivanje na bazu
const databeaseString = "mongodb://localhost:27017/WALKWITHJOYDB";
mongoose.connect(databeaseString, {
    useNewUrlParser:true,
    useUnifiedTopology:true
});

//otvaranje konekcije
mongoose.connection.once('open', function(){
    console.log('Uspesno povezivanje sa bazom');
});

//greska pri konektovanju
mongoose.connection.on('error', error =>{
    console.log('Greska: ', error);
})

app.use(json());
app.use(urlencoded({extended: false}));
  
// Implementacija CORS zastite
app.use(function (req, res, next) {
    res.header('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Headers', 'Content-Type');
  
    if (req.method === 'OPTIONS') {
        res.header(
        'Access-Control-Allow-Methods',
        'OPTIONS, GET, POST, PATCH, DELETE'
        );
        return res.status(200).json({});
    }
    next();
});

const usersRoutes = require('./routes/api/users');
const adminsRoutes = require('./routes/api/admins');
const walkersRoutes = require('./routes/api/walkers');
const requestsRoutes = require('./routes/api/requests');

app.use('/api/users', usersRoutes);
app.use('/api/admins', adminsRoutes);
app.use('/api/walkers', walkersRoutes);
app.use('/api/requests', requestsRoutes);

app.use(function (req, res, next){
    const error = new Error('Zahtev nije podrzan');
    error.status = 405;

    next(error);
});

app.use(function (error, req, res, next){
    const staticCode  = error.status || 500;
    res.status(staticCode).json({
        error: {
            message: error.message,
            status: staticCode,
            stack: error.stack
        },
    });
});

module.exports = app;