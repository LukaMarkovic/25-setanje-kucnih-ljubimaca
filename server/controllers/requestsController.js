const Request = require('../models/requestsModel')
const mongoos = require('mongoose');

module.exports.getRequests = async (req, res, next) => {

    try {
        const requests = await Request.find({}).exec();
        res.status(200).json(requests);
    } catch (err) {
        next(err);
    }
};


module.exports.getRequestById = async (req, res, next) => {

    try {
        const request = await Request.findById(req.params.id).exec();
        if (request) {
            res.status(200);
            res.json(request);
        } else {
            res.status(404);
            res.send();
        }
    } catch (err) {
        next(err);
    }
};


module.exports.getRequestForOwner = async (req, res, next) => {

    try {
        const request = await Request.find({ ownerId: req.params.id });
        if (request) {
            res.status(200);
            res.json(request);
        } else {
            res.status(404);
            res.send();
        }
    } catch (err) {
        next(err);
    }
};


module.exports.createRequest = async (req, res, next) => {
    if (!req.body.timeDate || !req.body.ownerId || !req.body.petId) {
        res.status(400);
        res.send();
    } else {
        try {
            const newRequest = new Request({
                _id: new mongoos.Types.ObjectId(),
                timeDate: req.body.timeDate,
                ownerId: req.body.ownerId,
                petId: req.body.petId,
                walkerId: null
            });

            await newRequest.save();
            res.status(201).json(newRequest);
        } catch (err) {
            next(err);
        }
    }

};
module.exports.updateRequestById = async (req,res, next)=>{
    
    try{
        const request = await Request.findById(req.params.id).exec();
        if(request){
            const body = req.body;
            if(body.walkerId){
                request.walkerId = body.walkerId;
                await request.save();
                res.status(200).send();
            }
        } else {
            res.status(404).send();
        }
    } catch (err) {
        next(err);
    }
};

module.exports.deleteRequestById = async (req, res, next) => {

    try {
        const request = await Request.findById(req.params.id).exec();
        if (request) {
            await Request.findByIdAndDelete(req.params.id).exec();
            res.status(200).send();
        } else {
            res.status(404).send();
        }
    } catch (err) {
        next(err);
    }
};

module.exports.deleteRequest = async (req, res, next)=>{
    try{
        const body = req.body;
        if(body.petId){
            await Request.deleteMany(
                {petId : body.petId}
            );
            res.status(200).send();
        } else if(body.ownerId){
            await Request.deleteMany(
                {ownerId : body.ownerId}
            );
            res.status(200).send();
        } else if(body.walkerId){
            await Request.updateMany(
                { walkerId: body.walkerId },
                { $set: { walkerId: null } }
            );
            res.status(200).send();
        }
    }catch(err){
        next(err);
    }
};

module.exports.deleteWalkerById = async (req, res, next) => {

    try {
        const request = await Request.findById(req.params.id).exec();
        if (request) {
            await Request.updateOne(
                { walkerId: req.body.walkerId },
                { $set: { walkerId: null } }
            );
            res.status(200).send();
        } else {
            res.status(404).send();
        }
    } catch (err) {
        next(err);
    }
};
