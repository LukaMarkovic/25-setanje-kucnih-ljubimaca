const User = require('../models/usersModel')
const mongoos = require('mongoose');

module.exports.getUsers = async (req, res, next) => {

    try {
        const users = await User.find({}).exec();
        res.status(200).json(users);
    } catch (err) {
        next(err);
    }
};

module.exports.getUserById = async (req, res, next) => {

    try {
        const user = await User.findById(req.params.id).exec();
        if (user) {
            res.status(200);
            res.json(user);
        } else {
            res.status(404);
            res.send();
        }
    } catch (err) {
        next(err);
    }
};

module.exports.createUser = async (req, res, next) => {

    try {
        if (!req.body.name || !req.body.surname || !req.body.password || !req.body.email) {
            if (!req.body.email || !req.body.password) {
                if (!req.body.email) {
                    res.status(400);
                    res.send();
                } else {
                    const user = await User.find({ email: req.body.email });
                    if (user) {
                        res.status(200).json(user);
                    } else {
                        res.status(400);
                        res.send();
                    }
                }
            } else {
                const user = await User.find({ email: req.body.email, password: req.body.password });
                if (user) {
                    res.status(200).json(user);
                } else {
                    res.status(400);
                    res.send();
                }
            }
        } else {
            const newUser = new User({
                _id: new mongoos.Types.ObjectId(),
                name: req.body.name,
                surname: req.body.surname,
                email: req.body.email,
                address: req.body.address,
                password: req.body.password
            });

            await newUser.save();
            res.status(201).json(newUser);
        }
    }catch (err) {
        next(err);
    }
};

module.exports.updateUserById = async (req, res, next) => {
    
    try {
        const user = await User.findById(req.params.id).exec();
        if (user) {
            const body = req.body;
            if (body.currentPassword && body.newPassword) {
                if (body.currentPassword != user.password) {
                    res.status(200).json('false');
                } else {
                    user.password = body.newPassword;
                    await user.save();
                    res.status(200).json('true');
                }
            }
        } else {
            res.status(404).send();
        }
    } catch (err) {
        next(err);
    }
};

module.exports.deleteUserById = async (req, res, next) => {


    try {
        const user = await User.findById(req.params.id).exec();
        if (user) {
            await User.findByIdAndDelete(req.params.id).exec();
            res.status(200).send();
        } else {
            res.status(404).send();
        }
    } catch (err) {
        next(err);
    }
};



module.exports.addUserPet = async (req, res, next) => {

    try {
        const user = await User.findById(req.params.id).exec();
        if (user) {
            const body = req.body;
            const newPet = {
                _id: new mongoos.Types.ObjectId(),
                name: body.name,
                breed: body.breed,
                bday: body.bday
            };
            user.pets.push(newPet);
            await user.save();
            res.status(200).json(user);
        } else {
            res.status(404).send();
        }
    } catch (err) {
        next(err);
    }
};

module.exports.removePetById = async (req, res, next) => {

    try {
        const user = await User.findById(req.params.userId).exec();
        if (user) {
            await User.updateOne(
                { _id: req.params.userId },
                { $pull: { pets: { _id: req.params.petId } } }
            )
            res.status(200).send();
        } else {
            res.status(404);
            res.send();
        }
    } catch (err) {
        next(err);
    }
};


