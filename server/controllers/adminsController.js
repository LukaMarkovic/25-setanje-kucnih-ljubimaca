const Admin = require('../models/adminsModel')
const mongoos = require('mongoose');

module.exports.getAdmins = async (req, res, next)=>{
    
    try{        
        const admins = await Admin.find({}).exec();
        res.status(200).json(admins);
    }catch(err){
        next(err);
    }
};


module.exports.getAdminById = async (req,res, next) =>{

    try{
        const admin = await Admin.findById(req.params.id).exec();
        if(admin){
            res.status(200);
            res.json(admin);
        } else {
            res.status(404);
            res.send();
        }
    }catch(err){
        next(err);
    }
};


module.exports.findAdminByEmail = async (req,res, next) => {
    
    try{
        if(!req.body.email || !req.body.password){
            if(!req.body.email){
                res.status(400);
                res.send();
            } else{
                const admin = await Admin.find({email: req.body.email});
                if(admin) {
                    res.status(200).json(admin);
                } else {
                    res.status(400);
                    res.send();
                }
            }
        }   else {
            const admin = await Admin.find({email: req.body.email, password: req.body.password});
            if(admin) {
                res.status(200).json(admin);
            } else {
                res.status(400);
                res.send();
            }
        }
    }catch(err){
        next(err);
    }
}


