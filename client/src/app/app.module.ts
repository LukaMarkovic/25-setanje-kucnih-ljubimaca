import { BrowserModule } from '@angular/platform-browser';
import { NgModule, APP_INITIALIZER } from '@angular/core';
import { ReactiveFormsModule} from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { NavigationComponent } from './navigation/navigation.component';
import { HomePageComponent } from './home-page/home-page.component';
import { AboutComponent } from './about/about.component';
import { UserListComponent } from './user-list/user-list.component';
import { UserInfoComponent } from './user-info/user-info.component';
import { AdminPageComponent } from './admin-page/admin-page.component';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';
import { ProfilComponent } from './profil/profil.component';
import { appInitializer } from './app.initializer';
import { CurrentUserService } from './services/current-user.service';
import { PetInfoComponent } from './pet-info/pet-info.component';
import { SignInSignUpComponent } from './sign-in-sign-up/sign-in-sign-up.component';
import { OwnerHomePageComponent } from './owner-home-page/owner-home-page.component';
import { AngularDateTimePickerModule } from 'angular2-datetimepicker';
import { OwnersService } from './services/owner.service';
import { AdminsService } from './services/admin.service';
import { WalkersService } from './services/walker.service';
import { WalkerHomePageComponent } from './walker-home-page/walker-home-page.component';

@NgModule({
  declarations: [
    AppComponent,
    NavigationComponent,
    HomePageComponent,
    AboutComponent,
    UserListComponent,
    UserInfoComponent,
    AdminPageComponent,
    PageNotFoundComponent,
    ProfilComponent,
    PetInfoComponent,
    SignInSignUpComponent,
    OwnerHomePageComponent,
    WalkerHomePageComponent
  ],
  imports: [
    BrowserModule,
    ReactiveFormsModule,
    HttpClientModule,
    AppRoutingModule,
    AngularDateTimePickerModule
  ],
  providers: [
    CurrentUserService,
    { provide: APP_INITIALIZER, useFactory: appInitializer, multi: true,
      deps: [CurrentUserService, OwnersService, AdminsService, WalkersService] }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
