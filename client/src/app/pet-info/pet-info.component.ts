import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Pet } from '../modules/pet.model';
import { CurrentUserService } from '../services/current-user.service';

@Component({
  selector: 'app-pet-info',
  templateUrl: './pet-info.component.html',
  styleUrls: ['./pet-info.component.scss']
})
export class PetInfoComponent implements OnInit {

  public pet: Pet;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private currentUserService: CurrentUserService
  ) {
    this.findPetById();
  }
  ngOnInit() {
  }


  private findPetById() {
    this.route.params.subscribe(par => {
      this.pet = this.currentUserService.getPetById(par.petId);
    });
  }

  private removePet() {
    this.currentUserService.removePet(this.pet);
    this.router.navigate(['/profil']);
  }

}
