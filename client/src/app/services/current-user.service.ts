import { Injectable } from '@angular/core';
import { User, Walker, Owner } from '../modules/user.model';
import { OwnersService } from './owner.service';
import { Observable, BehaviorSubject } from 'rxjs';
import { WalkersService } from './walker.service';
import { Pet } from '../modules/pet.model';
import { Router } from '@angular/router';
import { AdminsService } from './admin.service';

@Injectable({
  providedIn: 'root'
})
export class CurrentUserService {

  private currentUser = new BehaviorSubject<User>(null);
  private typeOfUser = new BehaviorSubject<string>('');
  private isUser = false;

  constructor(private ownerService: OwnersService,
              private walkerService: WalkersService,
              private adminService: AdminsService) {
  }

  public userLogin(): boolean {
    return this.isUser;
  }

  public getCurrentUser(): Observable<User> {
    return this.currentUser;
  }

  public getType(): Observable<string> {
    return this.typeOfUser;
  }

  public setTypeOfUser(type: string) {
    this.typeOfUser.next(type);
    localStorage.setItem('type', type);
  }

  public setCurrentUserExist(data) {
    this.currentUser.next(data);
    localStorage.setItem('token', JSON.stringify(data));
    this.isUser = true;
    if (data === null) {
      this.isUser = false;
    }
  }

  public async setCurrentUser(email: string, password: string) {
    await this.ownerService.login(email, password).toPromise().then((owner: Owner) => {
      if (owner[0]) {
        this.setTypeOfUser('owner');
        this.currentUser.next(owner[0]);
        localStorage.setItem('token', JSON.stringify(owner[0]));
        this.isUser = true;
      }
    });
    if (!this.currentUser.value) {
      await this.walkerService.login(email, password).toPromise().then((walker: Walker) => {
        if (walker[0]) {
          this.setTypeOfUser('walker');
          this.currentUser.next(walker[0]);
          localStorage.setItem('token', JSON.stringify(walker[0]));
          this.isUser = true;
        }
      });
    }
    if (!this.currentUser.value) {
      await this.adminService.login(email, password).toPromise().then((admin: User) => {
        if (admin[0]) {
          this.setTypeOfUser('admin');
          this.currentUser.next(admin[0]);
          localStorage.setItem('token', JSON.stringify(admin[0]));
          this.isUser = true;
        }
      });
    }
    if (!this.currentUser.value) {
      window.alert('wrong email or password');
    }
  }

  public logout() {
    localStorage.clear();
    this.currentUser.next(null);
    this.isUser = false;
    this.setTypeOfUser('');
  }


  public updatePassword(data) {
    if (this.typeOfUser.value === 'owner') {
      return this.ownerService.updateUserPassword(this.currentUser.value._id, data);
    } else if (this.typeOfUser.value === 'walker') {
      return this.walkerService.updateUserPassword(this.currentUser.value._id, data);
    }
  }

  public removeProfile(): boolean {
    if (window.confirm('Are you sure you want to delete account?')) {
      if (this.typeOfUser.value === 'owner') {
        const id = this.currentUser.value._id;
        this.logout();
        this.ownerService.removeUserById(id).subscribe();
      } else if (this.typeOfUser.value === 'walker') {
        const id = this.currentUser.value._id;
        this.logout();
        this.walkerService.removeUserById(id).subscribe();
      }
      return true;
    }
    return false;
  }

  public addPet(data) {
    this.ownerService.addPet(this.currentUser.value._id, data).subscribe(user => {
      this.currentUser.next(user);
    });
  }

  public getPetById(petId: string): Pet {
    let pet: Pet;
    (this.currentUser.value as Owner).pets.filter(p => {
      if (p._id === petId) {
        pet = p;
      }
    });
    return pet;
  }

  public removePet(pet: Pet) {
    this.ownerService.removePet(this.currentUser.value._id, pet._id).subscribe();
    (this.currentUser.value as Owner).pets.forEach((element, index) => {
      if (element._id === pet._id) {
        (this.currentUser.value as Owner).pets.splice(index, 1);
      }
    });
  }

}
