import { Injectable } from '@angular/core';
import { Walker } from '../modules/user.model';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { RequestService } from './request.service';

@Injectable({
  providedIn: 'root'
})
export class WalkersService {

  private walkers: Observable<Walker[]>;
  private readonly walkersUrl = 'http://localhost:3000/api/walkers/';

  constructor(private httpSerivce: HttpClient,
              private requestService: RequestService
    ) {
    this.refreshWalkers();
   }

   private refreshWalkers(): Observable<Walker[]> {
     this.walkers = this.httpSerivce.get<Walker[]>(this.walkersUrl);
     return this.walkers;
   }

  public getWalkers(): Observable<Walker[]> {
    return this.walkers;
  }

  public getUserById(id: string): Observable<Walker> {
    return this.httpSerivce.get<Walker>(this.walkersUrl + id);
  }

  public login(email: string, password: string): Observable<Walker> {
    return this.httpSerivce.post<Walker>(this.walkersUrl, { email, password });
  }

  public checkIfExist(email: string): Observable<Walker> {
    return this.httpSerivce.post<Walker>(this.walkersUrl, {email});
  }


  public addUser(data) {
    return this.httpSerivce.post<Walker>(this.walkersUrl, data);
  }

   public removeUserById(id: string) {
    this.requestService.deleteRequest({walkerId: id});
    return this.httpSerivce.delete(this.walkersUrl + id);
  }

  public updateUserPassword(id: string, data) {
    return this.httpSerivce.post(this.walkersUrl + id, data);
  }

}
