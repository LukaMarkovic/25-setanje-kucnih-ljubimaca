import { Injectable } from '@angular/core';
import { Request } from '../modules/requests.model';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
    providedIn: 'root'
})
export class RequestService {

    private requests: Observable<Request[]>;
    private readonly requestsUrl = 'http://localhost:3000/api/requests/';

    constructor(private httpSerivce: HttpClient) {
        this.refreshRequests();
    }

    private refreshRequests(): Observable<Request[]> {
        this.requests = this.httpSerivce.get<Request[]>(this.requestsUrl);
        return this.requests;
    }

    public getRequests(): Observable<Request[]> {
        return this.requests;
    }

    public getRequestById(id: string): Observable<Request> {
        return this.httpSerivce.get<Request>(this.requestsUrl + id);
    }

    public createRequests(data) {
        return this.httpSerivce.post<Request>(this.requestsUrl, data);
    }

    public addWalker(requestId, walkerId) {
        return this.httpSerivce.post<Request>(this.requestsUrl + requestId, { walkerId });
    }

    public removeRequestById(id: string) {
        return this.httpSerivce.delete(this.requestsUrl + id);
    }

    public getRequestForOwner(id: string): Observable<Request[]> {
        return this.httpSerivce.get<Request[]>(this.requestsUrl + `owner/${id}`);
    }

    public removeWalker(requestId, walkerId) {
        return this.httpSerivce.post<Request>(this.requestsUrl + `${requestId}/delete`, { walkerId });
    }

    public deleteRequest(data) {
        return this.httpSerivce.post<Request>(this.requestsUrl + 'delete', data).subscribe();
    }

}
