import { Injectable } from '@angular/core';
import { User } from '../modules/user.model';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AdminsService {

  private admins: Observable<User[]>;
  private readonly adminsUrl = 'http://localhost:3000/api/admins/';

  constructor(private httpSerivce: HttpClient) {
    this.refreshAdmins();
   }

   private refreshAdmins(): Observable<User[]> {
     this.admins = this.httpSerivce.get<User[]>(this.adminsUrl);
     return this.admins;
   }

  public getAdmins(): Observable<User[]> {
    return this.admins;
  }

  public getAdminById(id: string): Observable<User> {
    return this.httpSerivce.get<User>(this.adminsUrl + id);
  }


  public checkIfExist(email: string): Observable<User> {
    return this.httpSerivce.post<User>(this.adminsUrl, {email});
  }

  public login(email: string, password: string): Observable<User> {
    return this.httpSerivce.post<User>(this.adminsUrl, { email, password });
  }
}
