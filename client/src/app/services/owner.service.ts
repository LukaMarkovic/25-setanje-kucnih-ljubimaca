import { Injectable } from '@angular/core';
import { Owner} from '../modules/user.model';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { RequestService } from './request.service';

@Injectable({
  providedIn: 'root'
})
export class OwnersService {

  private owners: Observable<Owner[]>;
  private readonly ownersUrl = 'http://localhost:3000/api/users/';

  constructor(private httpSerivce: HttpClient,
              private requestService: RequestService
    ) {
    this.refreshOwners();
   }

   private refreshOwners(): Observable<Owner[]> {
     this.owners = this.httpSerivce.get<Owner[]>(this.ownersUrl);
     return this.owners;
   }

  public addPet(id: string, data) {
    return this.httpSerivce.post<any>(this.ownersUrl + `${id}/pet`, data);
  }

  public removePet(userId: string, petId: string ) {
    this.requestService.deleteRequest({petId});
    return this.httpSerivce.delete<any>(this.ownersUrl + userId + '/' + petId);
  }

  public getOwners(): Observable<Owner[]> {
    return this.owners;
  }

  public getUserById(id: string): Observable<Owner> {
    return this.httpSerivce.get<Owner>(this.ownersUrl + id);
  }

  public login(email: string, password: string): Observable<Owner> {
    return this.httpSerivce.post<Owner>(this.ownersUrl, { email, password });
  }

  public checkIfExist(email: string): Observable<Owner> {
    return this.httpSerivce.post<Owner>(this.ownersUrl, {email});
  }


  public addUser(data) {
    return this.httpSerivce.post<Owner>(this.ownersUrl, data);
  }

  public removeUserById(id: string) {
    this.requestService.deleteRequest({ownerId: id});
    return this.httpSerivce.delete(this.ownersUrl + id);
  }

  public updateUserPassword(id: string, data) {
    return this.httpSerivce.post(this.ownersUrl + id, data);
  }

}
