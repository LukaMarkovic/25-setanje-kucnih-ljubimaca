import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-error-page',
  templateUrl: './error-page.component.html',
  styleUrls: ['./error-page.component.scss']
})
export class ErrorPageComponent implements OnInit {

  public message: string;
  public statusCode: string;

  constructor(private route: ActivatedRoute) {
    this.route.paramMap.subscribe(paramMap => {
      this.message = paramMap.get('message');
      this.statusCode = paramMap.get('statusCode');
    });
   }

  ngOnInit() {
  }

}
