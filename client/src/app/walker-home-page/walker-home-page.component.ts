import { Component, OnInit, Input } from '@angular/core';
import { RequestService } from '../services/request.service';
import { CurrentUserService } from '../services/current-user.service';
import { OwnersService } from '../services/owner.service';

@Component({
  selector: 'app-walker-home-page',
  templateUrl: './walker-home-page.component.html',
  styleUrls: ['./walker-home-page.component.scss']
})
export class WalkerHomePageComponent implements OnInit {

  public now;
  public requests;
  public myWalks: Request[];
  public currentUser;
  public owners;

  constructor(private requestsService: RequestService,
              private currentUserService: CurrentUserService,
              private ownerService: OwnersService) {
    this.now = new Date();
    this.requestsService.getRequests().subscribe((value) => {
      this.requests = value;
      this.requests.forEach((element, index) => {
        const d = new Date(element.timeDate);
        if (d < this.now) {
          this.requestsService.removeRequestById(element._id).subscribe();
        }
      });
    });

    this.ownerService.getOwners().subscribe(value => {
      this.owners = value;
    });

    currentUserService.getCurrentUser().subscribe((value) => {
      this.currentUser = value;
    });
  }

  ngOnInit() {
  }

  public acceptWalk(id) {
    this.requests.filter(request => {
      if (request._id === id) {
        request.walkerId = this.currentUser._id;
      }
    });
    this.requestsService.addWalker(id, this.currentUser._id).subscribe();

  }

  public deleteWalk(id) {
    if (window.confirm('Are you sure you want to cancel your walk?')) {
      this.requests.filter(request => {
        if (request._id === id) {
          delete request.walkerId;
        }
      });
      this.requestsService.removeWalker(id, this.currentUser._id).subscribe();
    }
  }

}
