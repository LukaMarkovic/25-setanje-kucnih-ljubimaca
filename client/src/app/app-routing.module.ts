import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AboutComponent } from './about/about.component';
import { HomePageComponent } from './home-page/home-page.component';
import { UserListComponent } from './user-list/user-list.component';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';
import { ProfilComponent } from './profil/profil.component';
import { LoggedInGuard } from './logged-in.guard';
import { PetInfoComponent } from './pet-info/pet-info.component';


const routes: Routes = [
  // http://localhost:4200/
  { path: '', component: HomePageComponent },
  // http://localhost:4200/login
  { path: 'about', component: AboutComponent},
  // http://localhost:4200/about
  { path: 'users', component: UserListComponent},
  // http://localhost:4200/profil
  { path: 'profil', component: ProfilComponent, canActivate: [LoggedInGuard]},
  // http://localhost:4200/pets/:petId
  { path: 'pets/:petId', component: PetInfoComponent},
  // http://localhost:4200/*
  { path: '**', component: PageNotFoundComponent}
  ];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
