import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { CurrentUserService } from '../services/current-user.service';
import { OwnersService } from '../services/owner.service';
import { WalkersService } from '../services/walker.service';
import { Walker, Owner, User } from '../modules/user.model';
import { AdminsService } from '../services/admin.service';

@Component({
  selector: 'app-sign-in-sign-up',
  templateUrl: './sign-in-sign-up.component.html',
  styleUrls: ['./sign-in-sign-up.component.scss']
})
export class SignInSignUpComponent implements OnInit {

  public loginForm: FormGroup;
  public signUpForm: FormGroup;
  public container = null;

  public selectedRole: string;
  public isOwner: boolean;

  constructor(private router: Router,
    private currentUserService: CurrentUserService,
    private formBuilder: FormBuilder,
    private ownerService: OwnersService,
    private walkerService: WalkersService,
    private adminService: AdminsService) {
    this.loginForm = this.formBuilder.group({
      email: ['', [Validators.required, Validators.email]],
      password: ['', [Validators.required]]
    });

    this.signUpForm = this.formBuilder.group({
      roleRadio: ['walker'],
      email: ['', [Validators.required, Validators.email]],
      password: ['', [Validators.required]],
      name: [''],
      surname: [''],
      address: ['']
    });
  }

  ngOnInit() {
  }

  public async submitFormSignIn(data) {
    await this.currentUserService.setCurrentUser(data.email, data.password);
  }

  public async submitFormSignUp(data) {
    if (!this.signUpForm.valid) {
      window.alert('Not valid');
    } else {
      if (data.roleRadio === 'walker') {
        let userExist;
        // proveri da li postoji owner sa tim mailom
        await this.ownerService.checkIfExist(this.signUpForm.value.email).toPromise().then((owner: Owner) => {
          if (owner[0]) {
            userExist = true;
          } else {
            userExist = false;
          }
        });

        // proveri da li postoji walker sa tim mailom
        if (!userExist) {
          await this.walkerService.checkIfExist(this.signUpForm.value.email).toPromise().then((walker: Walker) => {
            if (walker[0]) {
              userExist = true;
            } else {
              userExist = false;
            }
          });
        }

        if (!userExist) {
          await this.adminService.checkIfExist(this.signUpForm.value.email).toPromise().then((admin: User) => {
            if (admin[0]) {
              userExist = true;
            } else {
              userExist = false;
            }
          });
        }

        // ako ne postoji ni walker ni owner napravi novog walkera
        if (!userExist) {
          await this.walkerService
            .addUser(this.signUpForm.value)
            .subscribe((walker: Walker) => {
              window.alert('Successfully created account!');
              this.currentUserService.setTypeOfUser('walker');
              this.currentUserService.setCurrentUserExist(walker);
              this.signUpForm.reset();
              this.router.navigate(['/']);
            });
        } else {
          window.alert('Ther is user with that email!');
        }
      }
      if (data.roleRadio === 'owner') {

        let userExist;
        // proveri da li postoji owner sa tim mailom
        await this.ownerService.checkIfExist(this.signUpForm.value.email).toPromise().then((owner: Owner) => {
          if (owner[0]) {
            userExist = true;
          } else {
            userExist = false;
          }
        });

        // proveri da li postoji walker sa tim mailom
        if (!userExist) {
          await this.walkerService.checkIfExist(this.signUpForm.value.email).toPromise().then((walker: Walker) => {
            if (walker[0]) {
              userExist = true;
            } else {
              userExist = false;
            }
          });
        }

        if (!userExist) {
          await this.adminService.checkIfExist(this.signUpForm.value.email).toPromise().then((admin: User) => {
            if (admin[0]) {
              userExist = true;
            } else {
              userExist = false;
            }
          });
        }
        // ako ne postoji ni walker ni owner napravi novog ownera
        if (!userExist) {
          await this.ownerService
            .addUser(this.signUpForm.value)
            .subscribe((owner: Owner) => {
              window.alert('Successfully created account!');
              this.currentUserService.setTypeOfUser('owner');
              this.currentUserService.setCurrentUserExist(owner);
              this.signUpForm.reset();
              this.router.navigate(['/']);
            });
        } else {
          window.alert('Ther is user with that email!');
        }
      }
    }
  }

  public radioChangeHandler(event: any) {
    this.selectedRole = event.target.value;
    if (this.selectedRole === 'walker') {
      this.isOwner = false;
    } else {
      this.isOwner = true;
    }
  }

  public signUpButton(): void {
    if (this.container === null) {
      this.container = document.getElementById('container');
    }
    this.container.classList.add('right-panel-active');
  }


  public signInButton(): void {
    if (this.container === null) {
      this.container = document.getElementById('container');
    }
    this.container.classList.remove('right-panel-active');
  }

}
