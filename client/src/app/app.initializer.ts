import { CurrentUserService } from './services/current-user.service';
import { OwnersService } from './services/owner.service';
import { AdminsService } from './services/admin.service';
import { WalkersService } from './services/walker.service';

export function appInitializer(currentUser: CurrentUserService,
                               ownerService: OwnersService,
                               adminService: AdminsService,
                               walkerService: WalkersService) {
    return () => new Promise(resolve => {
        // localStorage.clear();
        const user = localStorage.getItem('token');
        const type = localStorage.getItem('type');
        if (user) {
            if (type === 'owner') {
              ownerService.getUserById(JSON.parse(user)._id).subscribe(x => {
                currentUser.setTypeOfUser(type);
                currentUser.setCurrentUserExist(x);
                resolve();
              });
            } else if (type === 'walker') {
              walkerService.getUserById(JSON.parse(user)._id).subscribe(x => {
                currentUser.setTypeOfUser(type);
                currentUser.setCurrentUserExist(x);
                resolve();
              });
            } else if (type === 'admin') {
              adminService.getAdminById(JSON.parse(user)._id).subscribe(x => {
                currentUser.setTypeOfUser(type);
                currentUser.setCurrentUserExist(x);
                resolve();
              });
            }
          }
        if (type === '') {
          currentUser.setTypeOfUser('');
          currentUser.setCurrentUserExist(null);
          resolve();
        }
    });
}
