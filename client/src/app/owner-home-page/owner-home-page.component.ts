import { Component, OnInit } from '@angular/core';
import { CurrentUserService } from '../services/current-user.service';
import { RequestService } from '../services/request.service';

@Component({
  selector: 'app-owner-home-page',
  templateUrl: './owner-home-page.component.html',
  styleUrls: ['./owner-home-page.component.scss']
})
export class OwnerHomePageComponent implements OnInit {

  public now;
  public date;
  public selectedPet;

  settings = {
    bigBanner: true,
    timePicker: true,
    format: 'dd/MM/yyyy hh:mm',
    defaultOpen: false,
    closeOnSelect: true
  };
  public currentUser;
  public requests;

  constructor(private currentUserService: CurrentUserService,
              private requestsService: RequestService) {
    this.now = new Date();
    this.currentUserService.getCurrentUser().subscribe((value) => {
      this.currentUser = value;
    });

    this.requestsService.getRequestForOwner(this.currentUser._id).subscribe((value) => {
      this.requests = value;
      this.requests.forEach((element) => {
        const d = new Date(element.timeDate);
        if (d < this.now) {
          this.requestsService.removeRequestById(element._id).subscribe();
        }
      });
    });
  }

  ngOnInit() {
  }

  public requestWalk() {
    this.selectedPet = document.getElementById('pets');
    if (this.date) {
      const lastPosition = this.date.lastIndexOf(':');
      const s = this.date.substring(0, lastPosition);
      const d = new Date(s);
      if (this.now > d) {
        window.alert('Date has already passed!');
      } else {
        this.requestsService.createRequests({ownerId: this.currentUser._id, petId: this.selectedPet.value, timeDate: s}).
        subscribe(value => {
          this.requests.push(value);
        });
      }
    } else {
      window.alert('First chose date!');
    }
  }

  public deleteWalk(id) {
    this.requestsService.removeRequestById(id).subscribe();
    this.requests.forEach((element, index) => {
      if (element._id === id) {
        this.requests.splice(index, 1);
      }
    });
  }

  public cancelWalk() {
    window.alert('you can not cancel walk now :(' );
  }
}
