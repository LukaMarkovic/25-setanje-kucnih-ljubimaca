import { Component, OnInit } from '@angular/core';
import { CurrentUserService } from '../services/current-user.service';
import { User } from '../modules/user.model';

@Component({
  selector: 'app-home-page',
  templateUrl: './home-page.component.html',
  styleUrls: ['./home-page.component.scss']
})
export class HomePageComponent implements OnInit {

  private currentUser: User;
  private typeOfCurrentUser: string;

  constructor(private currentUserService: CurrentUserService) {
    currentUserService.getCurrentUser().subscribe((value) => {
      this.currentUser = value;
    });
    currentUserService.getType().subscribe((value) => {
      this.typeOfCurrentUser = value;
    });
  }

  ngOnInit() {
  }

}
