import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Pet } from '../modules/pet.model';

@Component({
  selector: 'app-user-info',
  templateUrl: './user-info.component.html',
  styleUrls: ['./user-info.component.scss']
})


export class UserInfoComponent implements OnInit {

  @Input() name: string;
  @Input() surname: string;
  @Input() pets: Pet[];
  @Input() id: string;

  // tslint:disable-next-line: no-output-rename
  @Output('delete') deleteEvent: EventEmitter<void> = new EventEmitter<void>();

  constructor() {

  }

  public deleteUser(id) {
    this.deleteEvent.emit();
  }

  ngOnInit() {
  }

}
