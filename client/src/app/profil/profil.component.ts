import { Component, OnInit } from '@angular/core';
import { CurrentUserService } from '../services/current-user.service';
import { User} from '../modules/user.model';
import { Router } from '@angular/router';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';


@Component({
  selector: 'app-profil',
  templateUrl: './profil.component.html',
  styleUrls: ['./profil.component.scss']
})
export class ProfilComponent implements OnInit {

  public currentUser: User;
  public type = '';
  public addPetForm: FormGroup;
  public changePasswordForm: FormGroup;
  public petForm = false;

  constructor(private router: Router,
              private currentUserService: CurrentUserService,
              private formBuilder: FormBuilder) {
    currentUserService.getCurrentUser().subscribe(value => {
      this.currentUser = value;
    });

    currentUserService.getType().subscribe(value => {
      this.type = value;
    });

    this.addPetForm = this.formBuilder.group({
      name: ['', [Validators.required]],
      breed: ['', [Validators.required]],
      bday: ['', [Validators.required]]
    });
    this.changePasswordForm = this.formBuilder.group({
      currentPassword: ['', [Validators.required]],
      newPassword: ['', [Validators.required]],
      confirmPassword: ['', [Validators.required]]
    });
   }

  ngOnInit() {
  }

  public submitPetForm(data): void {
    this.openPetForms();
    this.currentUserService.addPet(data);
    this.addPetForm.reset();
  }

  public submitPasswordForm(data): void {
    if (data.newPassword === data.confirmPassword) {
      this.currentUserService.updatePassword(data).subscribe(isOk => {
        if (isOk === 'true') {
            window.alert('Password successfully changed!');
          } else {
            window.alert('Wrong password');
          }
      });
      this.changePasswordForm.reset();
    } else {
      window.alert('The new password and confirmation password do not match!');
    }
  }


  public openPetForms(): void {
    this.petForm = !this.petForm;
  }

  public removeProfile(): void {
    if (this.currentUserService.removeProfile()){
      this.router.navigate(['/']);
    }
  }

}
