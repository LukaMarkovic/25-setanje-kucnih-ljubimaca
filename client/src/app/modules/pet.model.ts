export interface Pet {
    _id: string;
    name: string;
    breed: string;
    bday: number;
}

