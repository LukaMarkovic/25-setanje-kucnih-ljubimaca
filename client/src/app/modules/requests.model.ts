export interface Request {
    _id: string;
    timeDate: Date;
    ownerId: string;
    walkerId: string;
    petId: string;
}


