# Project 25-Šetanje-kućnih-ljubimaca

Aplikacija pomaže da vlasnici pasa nađu osobe koje bi šetale njihove ljubimce. 

1. Pokrenuti skript ```instalacija.sh``` za instalaciju svih potrebnih paketa i pravljenje baze
2. Pozicionirati se u direktorijum ```./server``` i u njemu izvršiti komandu ```nodemon server.js```
3. Pozicionirati se u direktorijum ```./client``` i u njemu izvršiti komandu  ```ng serve```
4. Pokrenuti aplikaciju na ```http://localhost:4200```

admin email: admin@gmail.com\
admin password: admin

## Developers


- [Luka Markovic , 423/2016](https://gitlab.com/LukaMarkovic)
- [Tamara Radovanović , 430/2016](https://gitlab.com/TamaraRado)
